﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour
{

    public int nBees = 50;

    public float beePeriod;
    public float minBeePeriod = 1;
    public float maxBeePeriod = 5;

    int i;

    private float time;


    public BeeMove beePrefab;
    public Rect spawnRect;
    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }
    //Sets the random time between minTime and maxTime
    void SetRandomTime()
    {
        beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
    }
    // Use this for initialization
    void Start()
    {

        SetRandomTime();
        for (i = 0; i < nBees; i++)
        {
            spawnBee();
        }
        time = minBeePeriod;
    }    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // Fixed bug by adding a type conversion
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }

    void spawnBee()
    {
        time = 0;

        // create bees
        
        
            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);
            // attach to this object in the hierarchy
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + i;

            // move the bee to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);
        
    }

    // Update is called once per frame
    void Update()
    {
        beePeriod = Random.Range(minBeePeriod, maxBeePeriod);

        //Counts up
        time += Time.deltaTime;

        //Check if its the right time to spawn the object
        if (time >= beePeriod)
        {
            spawnBee();
            SetRandomTime();
        }


       
    }
}